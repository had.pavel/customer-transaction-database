<?php

declare(strict_types=1);

namespace App\Presenters;

use App\CustomerOrdersModule\Repository\Helpers\SearchCriteriaHelper;
use App\CustomerOrdersModule\Repository\TransactionRepository;
use App\CustomerOrdersModule\Repository\UserRepository;

final class CustomerPresenter extends BasePresenter
{

    const RESULTS_PER_PAGE = 10;

    /**
     * @var UserRepository
     */
    private UserRepository $userRepository;

    /**
     * @var TransactionRepository
     */
    private TransactionRepository $transactionRepository;

    /**
     * @param UserRepository $userRepository
     * @param TransactionRepository $transactionRepository
     */
    public function __construct(
        UserRepository $userRepository,
        TransactionRepository $transactionRepository
    )
    {
        $this->userRepository = $userRepository;
        $this->transactionRepository = $transactionRepository;
    }

    public function actionDefault(string $offset = '0')
    {
        $searchCriteria = new SearchCriteriaHelper();
        $searchCriteria
            ->setLimit($this::RESULTS_PER_PAGE)
            ->setOffset(intval($offset) * $this::RESULTS_PER_PAGE);

        $users = $this->userRepository->find($searchCriteria);

        $this->template->customers = $users->toArray();
        $this->template->customersCount = $this->userRepository->getCount();
        $this->template->offset = $offset;
        $this->template->resultsPerPage = $this::RESULTS_PER_PAGE;
    }

}