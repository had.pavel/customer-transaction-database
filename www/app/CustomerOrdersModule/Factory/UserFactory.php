<?php

declare(strict_types=1);

namespace App\CustomerOrdersModule\Factory;

use App\CustomerOrdersModule\Models\ModelInterface;
use Doctrine\Common\Collections\Collection;
use Nette\Database\Row;
use App\CustomerOrdersModule\Models\UserModel;
use App\CustomerOrdersModule\Repository\Constants\UserDatabaseFields;

final class UserFactory extends BaseFactory implements FactoryInterface
{

    /**
     * @inheritDoc
     */
    public static function createFromDatabaseData(Row $data, ?Collection $additionalData = null): ?ModelInterface
    {

        foreach (UserDatabaseFields::USER_FIELDS as $field) {
            if (!isset($data[$field])) {
                return null;
            }
        }

        return new UserModel(
            $data->id,
            $data->password,
            $data->name,
            $data->address,
            $data->phone,
            $data->email,
            $data->created_at,
            $data->updated_at,
            $additionalData
        );

    }

}