<?php

declare(strict_types=1);

namespace App\CustomerOrdersModule\Factory;

use App\CustomerOrdersModule\Models\ModelInterface;
use App\CustomerOrdersModule\Models\TransactionModel;
use App\CustomerOrdersModule\Repository\Constants\TransactionDatabaseFields;
use Doctrine\Common\Collections\Collection;
use Nette\Database\Row;

final class TransactionFactory extends BaseFactory implements FactoryInterface
{

    /**
     * @inheritDoc
     */
    public static function createFromDatabaseData(Row $data, ?Collection $additionalData = null): ?ModelInterface
    {

        foreach (TransactionDatabaseFields::TRANSACTION_FIELDS as $field) {
            if (!isset($data[$field])) {
                return null;
            }
        }

        return new TransactionModel(
            $data->id,
            $data->user_id,
            $data->price,
            $data->currency,
            $data->created_at,
            $data->updated_at
        );

    }

}