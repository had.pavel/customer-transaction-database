<?php

declare(strict_types=1);

namespace App\CustomerOrdersModule\Factory;

use App\CustomerOrdersModule\Models\ModelInterface;
use Doctrine\Common\Collections\Collection;
use Nette\Database\Row;

interface FactoryInterface
{

    /**
     * @param Row $data
     * @param Collection|null $additionalData
     * @return ModelInterface|null
     */
    public static function createFromDatabaseData(Row $data, ?Collection $additionalData = null): ?ModelInterface;

}