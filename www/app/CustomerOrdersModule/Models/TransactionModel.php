<?php

declare(strict_types=1);

namespace App\CustomerOrdersModule\Models;

use DateTime;

final class TransactionModel extends BaseModel implements ModelInterface
{
    private int $id;
    private int $userId;
    private float $price;
    private string $currency;

    /**
     * @var DateTime
     */
    private DateTime $created;

    /**
     * @var DateTime
     */
    private DateTime $updated;

    /**
     * @param int $id
     * @param int $userId
     * @param float $price
     * @param string $currency
     * @param DateTime $created
     * @param DateTime $updated
     */
    public function __construct(
        int $id,
        int $userId,
        float $price,
        string $currency,
        DateTime $created,
        DateTime $updated
    )
    {
        $this->id = $id;
        $this->userId = $userId;
        $this->price = $price;
        $this->currency = $currency;
        $this->created = $created;
        $this->updated = $updated;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     */
    public function setUserId(int $userId): void
    {
        $this->userId = $userId;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice(float $price): void
    {
        $this->price = $price;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     */
    public function setCurrency(string $currency): void
    {
        $this->currency = $currency;
    }

    /**
     * @return DateTime
     */
    public function getCreated(): DateTime
    {
        return $this->created;
    }

    /**
     * @param DateTime $created
     */
    public function setCreated(DateTime $created): void
    {
        $this->created = $created;
    }

    /**
     * @return DateTime
     */
    public function getUpdated(): DateTime
    {
        return $this->updated;
    }

    /**
     * @param DateTime $updated
     */
    public function setUpdated(DateTime $updated): void
    {
        $this->updated = $updated;
    }

}