<?php

declare(strict_types=1);

namespace App\CustomerOrdersModule\Models;

interface ModelInterface
{
    /**
     * @return int
     */
    public function getId(): int;
}