<?php

declare(strict_types=1);

namespace App\CustomerOrdersModule\Models;

abstract class BaseModel
{
    private int $id;

    /**
     * @param int $id
     */
    public function __construct(
        int $id
    )
    {
        $this->id = $id;
    }
}