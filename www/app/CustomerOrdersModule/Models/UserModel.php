<?php

declare(strict_types=1);

namespace App\CustomerOrdersModule\Models;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

final class UserModel extends BaseModel implements ModelInterface
{
    private int $id;
    private string $password;
    private string $name;
    private string $address;
    private string $phone;
    private string $email;

    /**
     * @var DateTime
     */
    private DateTime $created;

    /**
     * @var DateTime
     */
    private DateTime $updated;

    /**
     * @var ArrayCollection|TransactionModel[]|null
     */
    private ?ArrayCollection $transactions = null;

    /**
     * @param int $id
     * @param string $password
     * @param string $name
     * @param string $address
     * @param string $phone
     * @param string $email
     * @param DateTime $created
     * @param DateTime $updated
     * @param Collection $transactions
     */
    public function __construct(
        int        $id,
        string     $password,
        string     $name,
        string     $address,
        string     $phone,
        string     $email,
        DateTime   $created,
        DateTime   $updated,
        Collection $transactions
    )
    {
        $this->id = $id;
        $this->password = $password;
        $this->name = $name;
        $this->address = $address;
        $this->phone = $phone;
        $this->email = $email;
        $this->created = $created;
        $this->updated = $updated;
        $this->transactions = $transactions;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getAddress(): string
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress(string $address): void
    {
        $this->address = $address;
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone(string $phone): void
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return DateTime
     */
    public function getCreated(): DateTime
    {
        return $this->created;
    }

    /**
     * @param DateTime $created
     */
    public function setCreated(DateTime $created): void
    {
        $this->created = $created;
    }

    /**
     * @return DateTime
     */
    public function getUpdated(): DateTime
    {
        return $this->updated;
    }

    /**
     * @param DateTime $updated
     */
    public function setUpdated(DateTime $updated): void
    {
        $this->updated = $updated;
    }

    /**
     * @return TransactionModel[]|ArrayCollection|Collection|null
     */
    public function getTransactions()
    {
        return $this->transactions;
    }

    /**
     * @return Collection|string[]
     */
    public function getPaidCurrencies(): Collection
    {
        $currencies = [];

        foreach ($this->getTransactions() as $transaction) {
            $currencies[$transaction->getCurrency()] = '';
        }

        return new ArrayCollection(array_keys($currencies));
    }

    /**
     * @param DateTime $from
     * @return Collection
     */
    public function getCurrencyPaidValuePair(DateTime $from): Collection
    {
        $currencies = [];

        foreach ($this->getTransactions() as $transaction) {
            if ($transaction->getCreated() > $from) {
                if (array_key_exists($transaction->getCurrency(), $currencies)) {
                    $currencies[$transaction->getCurrency()] += $transaction->getPrice();
                } else {
                    $currencies[$transaction->getCurrency()] = $transaction->getPrice();
                }
            }
        }

        return new ArrayCollection($currencies);
    }

}