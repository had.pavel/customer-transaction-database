<?php

declare(strict_types=1);

namespace App\CustomerOrdersModule\Repository\Helpers;

final class SearchCriteriaHelper
{
    private string $tableName;
    private ?string $where = null;
    private ?int $limit = 0;
    private ?int $offset = 0;

    /**
     * @return string
     */
    public function getTableName(): ?string
    {
        return $this->tableName;
    }

    /**
     * @return string|null
     */
    public function getWhere(): ?string
    {
        return $this->where;
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit;
    }

    /**
     * @return int
     */
    public function getOffset(): int
    {
        return $this->offset;
    }

    /**
     * @param string $tableName
     * @return SearchCriteriaHelper
     */
    public function setTableName(string $tableName): SearchCriteriaHelper
    {
        $this->tableName = $tableName;
        return $this;
    }

    /**
     * @param string|null $where
     * @return SearchCriteriaHelper
     */
    public function setWhere(?string $where): SearchCriteriaHelper
    {
        $this->where = $where;
        return $this;
    }

    /**
     * @param int|null $limit
     * @return SearchCriteriaHelper
     */
    public function setLimit(?int $limit): SearchCriteriaHelper
    {
        $this->limit = $limit;
        return $this;
    }

    /**
     * @param int|null $offset
     * @return SearchCriteriaHelper
     */
    public function setOffset(?int $offset): SearchCriteriaHelper
    {
        $this->offset = $offset;
        return $this;
    }

    /**
     * @return string
     */
    public function buildQueryString(): string
    {
        $query = 'SELECT * FROM ' . $this->getTableName();

        if (!is_null($this->getWhere())) {
            $query .= ' WHERE ' . $this->getWhere();
        }

        if ($this->getLimit() != 0) {
            $query .= ' LIMIT ' . $this->getLimit();
        }

        if ($this->getOffset() != 0) {
            $query .= ' OFFSET ' . $this->getOffset();
        }

        return $query;
    }

}