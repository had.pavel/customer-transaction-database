<?php

declare(strict_types=1);

namespace App\CustomerOrdersModule\Repository\Constants;

class UserDatabaseFields
{

    const USER_FIELDS = [
        'id',
        'password',
        'name',
        'address',
        'phone',
        'email',
        'created_at',
        'updated_at',
    ];

}