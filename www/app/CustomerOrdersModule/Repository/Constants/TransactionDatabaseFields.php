<?php

declare(strict_types=1);

namespace App\CustomerOrdersModule\Repository\Constants;

class TransactionDatabaseFields
{

    const TRANSACTION_FIELDS = [
        'id',
        'user_id',
        'price',
        'currency',
        'created_at',
        'updated_at'
    ];

}