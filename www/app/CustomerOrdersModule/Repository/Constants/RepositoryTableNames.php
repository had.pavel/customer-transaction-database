<?php

declare(strict_types=1);

namespace App\CustomerOrdersModule\Repository\Constants;

class RepositoryTableNames
{
    const USER_TABLE_NAME = 'users';
    const TRANSACTIONS_TABLE_NAME = 'transactions';
}