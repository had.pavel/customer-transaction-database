<?php

declare(strict_types=1);

namespace App\CustomerOrdersModule\Repository;

use App\CustomerOrdersModule\Models\ModelInterface;
use App\CustomerOrdersModule\Repository\Helpers\SearchCriteriaHelper;
use Doctrine\Common\Collections\Collection;

interface RepositoryInterface
{

    /**
     * @param int $id
     * @return ModelInterface|null
     */
    public function get(int $id): ?ModelInterface;

    /**
     * @param SearchCriteriaHelper $searchCriteria
     * @return Collection|null
     */
    public function find(SearchCriteriaHelper $searchCriteria): ?Collection;

}