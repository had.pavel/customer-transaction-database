<?php

declare(strict_types=1);

namespace App\CustomerOrdersModule\Repository;

use App\CustomerOrdersModule\Factory\FactoryInterface;
use App\CustomerOrdersModule\Models\ModelInterface;
use App\CustomerOrdersModule\Repository\Helpers\SearchCriteriaHelper;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Nette\Database\ResultSet;
use Nette\Database\Row;
use Nette\SmartObject;
use Nette\Database\Explorer;

abstract class BaseRepository implements RepositoryInterface
{
    use SmartObject;

    /**
     * @var Explorer
     */
    protected Explorer $database;

    /**
     * @param Explorer $database
     */
    public function __construct(Explorer $database)
    {
        $this->database = $database;
    }

    abstract public function getTableName(): string;

    abstract public function getFactoryInterface(): FactoryInterface;

    abstract public function getModel(): ModelInterface;

    abstract public function getAdditionalData(int $thisModelId): ?Collection;

    /**
     * @param SearchCriteriaHelper $searchCriteria
     * @return Row|null
     */
    protected function getRow(SearchCriteriaHelper $searchCriteria): ?Row
    {
        return $this->database->query($searchCriteria->buildQueryString())->fetch();
    }

    /**
     * @param SearchCriteriaHelper $searchCriteria
     * @return ResultSet
     */
    protected function getRows(SearchCriteriaHelper $searchCriteria): ResultSet
    {
        return $this->database->query($searchCriteria->buildQueryString());
    }

    /**
     * @inheritDoc
     */
    public function get(int $id): ?ModelInterface
    {
        $searchCriteria = new SearchCriteriaHelper();
        $searchCriteria
            ->setTableName($this->getTableName())
            ->setWhere('id = ' . $id);

        $fetchData = $this->getRow($searchCriteria);

        if (is_null($fetchData)) {
            return null;
        }

        return $this->getFactoryInterface()::createFromDatabaseData($fetchData, new ArrayCollection());
    }

    /**
     * @inheritDoc
     */
    public function find(SearchCriteriaHelper $searchCriteria): ?Collection
    {
        $modelCollection = new ArrayCollection();

        $searchCriteria->setTableName($this->getTableName());

        $dataRows = $this->getRows($searchCriteria);

        foreach ($dataRows as $dataRow) {
            $user = $this->getFactoryInterface()::createFromDatabaseData(
                $dataRow,
                $this->getAdditionalData($dataRow->id)
            );

            if (!is_null($user)) {
                $modelCollection->add($user);
            }
        }

        return $modelCollection;
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->database->table($this->getTableName())->count('*');
    }

}