<?php

declare(strict_types=1);

namespace App\CustomerOrdersModule\Repository;

use App\CustomerOrdersModule\Factory\FactoryInterface;
use App\CustomerOrdersModule\Factory\TransactionFactory;
use App\CustomerOrdersModule\Models\ModelInterface;
use App\CustomerOrdersModule\Models\TransactionModel;
use App\CustomerOrdersModule\Repository\Constants\RepositoryTableNames;
use Doctrine\Common\Collections\Collection;

final class TransactionRepository extends BaseRepository implements RepositoryInterface
{

    /**
     * @return string
     */
    public function getTableName(): string
    {
        return RepositoryTableNames::TRANSACTIONS_TABLE_NAME;
    }

    /**
     * @return FactoryInterface
     */
    public function getFactoryInterface(): FactoryInterface
    {
        return new TransactionFactory();
    }

    /**
     * @return ModelInterface
     */
    public function getModel(): ModelInterface
    {
        return TransactionModel::class;
    }

    /**
     * @param int $thisModelId
     * @return Collection|null
     */
    public function getAdditionalData(int $thisModelId): ?Collection
    {
        return null;
    }


}