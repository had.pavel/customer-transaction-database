<?php

declare(strict_types=1);

namespace App\CustomerOrdersModule\Repository;

use App\CustomerOrdersModule\Factory\FactoryInterface;
use App\CustomerOrdersModule\Factory\TransactionFactory;
use App\CustomerOrdersModule\Models\ModelInterface;
use App\CustomerOrdersModule\Models\UserModel;
use App\CustomerOrdersModule\Repository\Constants\RepositoryTableNames;
use App\CustomerOrdersModule\Factory\UserFactory;
use App\CustomerOrdersModule\Repository\Helpers\SearchCriteriaHelper;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

final class UserRepository extends BaseRepository implements RepositoryInterface
{

    /**
     * @return string
     */
    public function getTableName(): string
    {
        return RepositoryTableNames::USER_TABLE_NAME;
    }

    /**
     * @return FactoryInterface
     */
    public function getFactoryInterface(): FactoryInterface
    {
        return new UserFactory();
    }

    /**
     * @return ModelInterface
     */
    public function getModel(): ModelInterface
    {
        return UserModel::class;
    }


    /**
     * @param int $thisModelId
     * @return Collection|null
     */
    public function getAdditionalData(int $thisModelId): ?Collection
    {
        $modelCollection = new ArrayCollection();

        $searchCriteria = new SearchCriteriaHelper();
        $searchCriteria
            ->setTableName(RepositoryTableNames::TRANSACTIONS_TABLE_NAME)
            ->setWhere('user_id = ' . $thisModelId);

        $dataRows = $this->getRows($searchCriteria);

        foreach ($dataRows as $dataRow) {
            $transaction = TransactionFactory::createFromDatabaseData($dataRow);
            if (!is_null($transaction)) {
                $modelCollection->add($transaction);
            }
        }

        return $modelCollection;
    }

}