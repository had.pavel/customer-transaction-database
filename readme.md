# User-transaction

Dvě tabulky - v jedné je seznam uživatelů a ve druhé seznam plateb, které uživatelé udělali. Jeden uživatel mohl uskutečnit více plateb. Prosím, napiš v PHP skript, který propojí tyto dvě tabulky a vypíše u každého uživatele celkovou sumu, kterou uživatel zaplatil. Sumu vypiš pouze pro platby, které byly provedeny za poslední měsíc, pro každou měnu zvlášť.

## Installation

V hlavní složce projektu nastartujeme docker

```bash
docker compose up
```

Jdeme do www složky projektu a stáhneme potřebné závislosti

```bash
cd ./www
composer install
```

vložíme do databáze potřebné záznamy ze souboru pohovor.sql, například přes [adminer](http://localhost:8080/)

## Usage

Otevřeme si okno prohlížeče a [kocháme se tou nádherou](http://localhost:8081/)

## License
I don't mind